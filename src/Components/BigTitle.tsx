import React from "react";
import BigLogo from'/src/assets/LogoBig.svg'
import Draw from'/src/assets/Draw.svg'

const BigTitle =()=>{

    return(
        <div className={"d-flex align-items-center mt-2"} >
            <img src={Draw} />
            <img src={BigLogo} height={143} width={750}/>
        </div>
    )
}
export default BigTitle
