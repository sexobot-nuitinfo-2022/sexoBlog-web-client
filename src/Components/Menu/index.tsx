import React from "react";
import {Button, Nav} from "react-bootstrap";
import Logo from '/src/assets/logo.png'



const Menu=()=>{


    return(
        <div className="d-flex justify-content-around w-100 align-items-center">
            <div className={"p-2"}>
            <img src={Logo} alt={'logo'}/>
            </div>
            <div className={"p-2"}>
                <Nav >
                    <Nav.Item className={"p-2"}  >
                        <Nav.Link href="/home">Acceuil</Nav.Link>
                    </Nav.Item>
                    <Nav.Item className={"p-2"}>
                        <Nav.Link href="/about">A props</Nav.Link>
                    </Nav.Item>
                </Nav>
            </div>
            <div className={"p-2"}>
            <Button className={"btn btn-danger"} onClick={()=> window.open("https://discord.gg/9D2ZrgsV7f", "_blank")} >Rejoindre Discord</Button>
            </div>
        </div>
    )

}
export  default Menu
