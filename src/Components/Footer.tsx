import React from 'react'
import Logo from "/src/assets/logo.png";

const  Footer =()=>{
return(
    <div className={"d-flex flex-column align-items-center w-100 "}>
        <div className={"d-flex justify-content-between w-75 borderBottom "}>
            <img src={Logo} alt={'logo'}/>
            <p className={"footerText"}>
                Notre but est de sensibiliser non seulement les jeunes adultes
                mais aussi les plus âgés
            </p>
        </div>
        <div>
            <p className={"copyRightsText"}>
                Copyright © 2022 SexoBlog
            </p>
        </div>

    </div>
)

}
export  default Footer
