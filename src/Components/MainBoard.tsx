import React from "react";
import ArrowDraw from'/src/assets/ArrowDraw.svg'
import UnDraw from'/src/assets/undraw_learning.svg'
import {Button} from "react-bootstrap";

const MainBoard =()=>{

    return(
        <div className={"d-flex align-items-center mt-1 w-75 justify-content-around"} >
            <img src={UnDraw} width={418} height={333} />
            <Button className={"btn-danger"} >Découvrir</Button>
            <img src={ArrowDraw} />
        </div>
    )
}
export default MainBoard
