import React from "react";
import  AvatarReact  from "react-avatar";
import {Article} from "../types";

export type CardProps={
    item:Article

}
const CardElement =({item}:CardProps)=>{
    const {sender}=item
    const Avatar = <AvatarReact src={sender.displayAvatarURL}
                                name={sender.username} size={"40"} round="20px"  />
    return(
        <div style={{backgroundColor:item.backgoundColor ||'#FAF8F8'}} className={"p-4 card-layout m-xl-2 "}>
            <div className={"d-flex justify-content-between align-items-center"} >
                <div className={"d-flex "}>
                {Avatar}
                    <p className={"p-2 align-self-center articleName"} style={{color:item.textColor , fontFamily:item.textFont}} >
                        {sender.username}
                    </p>
                </div>
                <p className={"align-self-center articleNumber"} style={{color:item.textColor , fontFamily:item.textFont}}>
                    #{sender.discriminator}
                </p>
            </div>
            <div>
                <p className={"articleHeader"} style={{color:item.textColor , fontFamily:item.textFont}}> {item.title}</p>
                <p className={"articleDetail"} style={{color:item.textColor , fontFamily:item.textFont}}>
                    {item.content}
                </p>
            </div>
            <div className={'d-flex justify-content-between align-items-center'} >
                <p>
                </p>
                <p className={"articleDate"} style={{color:item.textColor , fontFamily:item.textFont}} >
                    {item.createdAt}
                </p>
            </div>
        </div>
    )
}
export default CardElement
