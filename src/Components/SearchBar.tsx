import React, {useState} from "react";
import SearchBlur from "/src/assets/search.svg"
import SearchFocus from "/src/assets/search_focus.svg"
import './style.css'

type SearchProps={
    handleChange: (val:string)=>void
    value:string
};

const SearchBar =(props:SearchProps)=>{
 const [focus,setFocus]=useState<boolean>(false)
 const handleChange=(val:string)=>{
     props.handleChange(val)
 }
    return(
        <div className={"d-flex border-round"}>
        <input
                value={props.value}
                type={"text"} placeholder={"Taper nom de l’article ...."} className={"searchText flex-grow-1"}
                onFocus={()=>setFocus(true)}
                onBlur={()=>setFocus(false)}
                onChange={(event)=>handleChange(event.target.value)}
        />
            <img src={focus?SearchFocus:SearchBlur}/>
        </div>
    )
}
export default SearchBar
