import React, {useEffect, useState} from 'react';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import SearchBar from "./SearchBar";
import Card from "./Card";
import axios from 'axios';
import {Article} from "../types";

const BaseUrl='http://137.184.76.192:60004'
const TabNavigator=()=> {
    const [key, setKey] = useState<string>('recent');
    const [searchValue,setSearchValue]=useState<string>('')
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState<Article[]>([])
    useEffect(() => {
        const fetchData = async () =>{
            setLoading(true);
            try {
                const {data: response} = await axios.get<Article[]>(BaseUrl+'/articles');
                setData(response);
                console.log(response)
            } catch (error) {
                console.error(error);
            }
            setLoading(false);
        }

        fetchData();
    }, [key]);
    return (
        <Tabs
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => {
                k && setKey(k);
            }}
            className="mb-4 mt-4"
        >
            <Tab eventKey="recent" title="Tout récent">
                <div className={'d-flex flex-column align-items-center w-100'}>
                    <SearchBar value={searchValue} handleChange={setSearchValue}/>
                    <div className={'d-flex flex-wrap w-75 justify-content-center mt-xl-5 mb-xl-5'}>
                        {data.map((elem)=>
                            <Card key={elem._id} item={elem}/>)}
                    </div>
                </div>
            </Tab>
            <Tab eventKey="journaliere" title="Journalière">
                <div className={'d-flex flex-column align-items-center w-100'}>
                <SearchBar value={searchValue} handleChange={setSearchValue}/>
                <div className={'d-flex flex-wrap w-75 justify-content-center mt-xl-5 mb-xl-5'}>
                    {data.map((elem)=>
                        <Card key={elem._id} item={elem}/>)}
                </div>
                </div>
            </Tab>
            <Tab eventKey="mensuel" title="Mensuel" >
                <div className={'d-flex flex-column align-items-center w-100'}>
                    <SearchBar value={searchValue} handleChange={setSearchValue}/>
                    <div className={'d-flex flex-wrap w-75 justify-content-center mt-xl-5 mb-xl-5'}>
                        {data.map((elem)=>
                            <Card key={elem._id} item={elem}/>)}
                    </div>
                </div>
            </Tab>
            <Tab eventKey="annuelle" title="Annuelle" >
                <div className={'d-flex flex-column align-items-center w-100'}>
                    <SearchBar value={searchValue} handleChange={setSearchValue}/>
                    <div className={'d-flex flex-wrap w-75 justify-content-center mt-xl-5 mb-xl-5'}>
                        {data.map((elem)=>
                            <Card key={elem._id} item={elem}/>)}
                    </div>
                </div>
            </Tab>
        </Tabs>
    );
}

export default TabNavigator;
