import Menu from "./Components/Menu";
import BigTitle from "./Components/BigTitle";
import './App.css'
import MainBoard from "./Components/MainBoard";
import TabNavigator from "./Components/TabNavigator";
import Footer from "./Components/Footer";

function App() {


  return (
      <div>
         <div className={'d-flex flex-column align-items-center v-100 w-100'}>
        <Menu/>
        <BigTitle />
        <p style={{fontFamily:"Poppins",fontStyle: "norma",fontWeight: 500,fontSize: "64px",marginTop:"-40px"}} >Education de la Santé sexuelle</p>
        <p className={"small-text"}>nous aimerions aider les plus jeunes à devenir acteurs de leur santé sexuelle</p>
        <MainBoard/>

    </div>
          <div className={'d-flex flex-column align-items-center mt-5'}>
              <p className={"title-text mt-5"}>Articles récents</p>
              <TabNavigator/>
             <Footer/>

          </div>
      </div>
  )
}

export default App
