export interface Article {
    _id: string;
    status: string;
    textFont: string;
    textColor: string;
    backgoundColor: string;
    content: string;
    title: string;
    sender: Sender;
    createdAt: string;
    updatedAt: string;
    __v: number;
}
export interface Sender {
    _id: string;
    role: string;
    displayAvatarURL: string;
    avatarURL: string;
    tag: string;
    defaultAvatarURL: string;
    createdTimestamp: string;
    avatar: string;
    discriminator: string;
    discordId: string;
    username: string;
    createdAt: string;
    __v: number;
}
